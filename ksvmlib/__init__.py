from .ksvm import KSVM as KSVM
from gplib import kernel_functions as ker
from gplib import fitting_methods as fit
from . import metrics as me
from gplib import data_manipulations as dm
from . import plot

__version__ = '0.1.1'