ksvmlib package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   ksvmlib.metrics

Submodules
----------

ksvmlib.ksvm module
-------------------

.. automodule:: ksvmlib.ksvm
   :members:
   :undoc-members:
   :show-inheritance:

ksvmlib.plot module
-------------------

.. automodule:: ksvmlib.plot
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ksvmlib
   :members:
   :undoc-members:
   :show-inheritance:
