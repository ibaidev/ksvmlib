.. kSVMlib documentation master file, created by
   sphinx-quickstart on Sun Jun 14 17:51:20 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to kSVMlib's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
