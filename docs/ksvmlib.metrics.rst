ksvmlib.metrics package
=======================

Submodules
----------

ksvmlib.metrics.accuracy module
-------------------------------

.. automodule:: ksvmlib.metrics.accuracy
   :members:
   :undoc-members:
   :show-inheritance:

ksvmlib.metrics.svmbic module
-----------------------------

.. automodule:: ksvmlib.metrics.svmbic
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ksvmlib.metrics
   :members:
   :undoc-members:
   :show-inheritance:
